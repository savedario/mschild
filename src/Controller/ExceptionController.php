<?php
/**
 *
 * User: dario
 * Date: 2019-06-11
 * Time: 10:00
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;


class ExceptionController extends AbstractFOSRestController
{
    public function restException($exception)
    {
        return new Response('Error: '.$exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
}