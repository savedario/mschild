<?php
/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 20:08
 */

namespace App\Util\Factor;

/**
 * Class FakeAlg
 *
 * A copy of DirectSearch to test multiple algorithms without having to figure out the math behind a real new one
 *
 * @package App\Util\Factor
 */
class FakeAlg extends AbstractPrimeFactor implements PrimeFactorInterface
{
    /**
     * Determine prime factor for an integer
     * Using Direct Search Factorization algorithm
     *
     * @param int $number
     * @return null|array of factors
     */
    public function primeFactorization(int $number ): array
    {
        if ($number > $this->max) {
            return [];
        }

        $current = $number;
        $result = [];
        for ($i = 2; $i <= $number; $i++) {
            $count = 1;
            while ( !($modulo = $current % $i) ) {
                $result[$i] = $count++;
                $current = $current / $i;
            }
            if ($current === 1) {
                break;
            }
        }
        return $result;
    }
}
