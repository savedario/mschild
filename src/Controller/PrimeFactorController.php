<?php

use App\Util\Factor\AbstractPrimeFactor;

/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 10:27
 */

namespace App\Controller;

use App\Util\Factor;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PrimeFactorController
 *
 * @package App\Controller
 *  @Rest\RouteResource("factor", pluralize=false)
 */
class PrimeFactorController extends AbstractFOSRestController implements ClassResourceInterface
{
    public function __construct()
    {
        new Factor\DirectSearch();
    }


    private function invalidInputResponse($message='Invalid input')
    {
        return new Response($message, Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @Rest\QueryParam(name="number", requirements="\d+", default="0", description="Number to process")
     * @Rest\QueryParam(name="alg", requirements="[a-z,A-Z]+", default="DirectSearch", description="Algorithm to use")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAction( ParamFetcher $paramFetcher ) // $number, $algorithm='DirectSearch' )
    {
        $number = $paramFetcher->get('number',true);
        $algorithm = $paramFetcher->get('alg',true);
        $algorithm = '\App\Util\Factor\\'.$algorithm;

        if (!class_exists($algorithm)) {
            return $this->invalidInputResponse('Algorithm not found.');
        }
        /* @var AbstractPrimeFactor $calculator */
        $calculator = new $algorithm();

        $invalid = $calculator->invalidInput( $number );
        if ($invalid) {
            return $this->invalidInputResponse( $invalid );
        }

        $result = [
            'status' => 'Ok',
            'factors' => $calculator->primeFactorization( $number )
        ];

        $view = $this->view($result, 200);
        return $this->handleView($view);
    }
}