<?php
/**
 *
 * User: dario
 * Date: 2019-06-10
 * Time: 19:12
 */

namespace App\Exception;


use Symfony\Component\Config\Definition\Exception\Exception;

class InvalidDataException extends Exception
{

}