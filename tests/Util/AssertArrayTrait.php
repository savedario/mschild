<?php

namespace App\Tests\Util;

use PHPUnit\Framework\ExpectationFailedException;

trait AssertArrayTrait {
    private function assertArray( $expect, $result )
    {
        try {
            $this->assertEquals(count($expect), count($result));
            foreach ($expect as $k => $v) {
                $this->assertEquals($v, $result[$k]);
            }
        } catch (ExpectationFailedException $e) {
            echo "\nExpected\n";
            foreach ($expect as $k => $v) {
                echo "{$k} x {$v}\n";
            }
            echo "Result\n";
            foreach ($result as $k => $v) {
                echo "{$k} x {$v}\n";
            }
            throw new ExpectationFailedException($e->getMessage());
        }
    }
}