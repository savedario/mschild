<?php
/**
 *
 * User: dario
 * Date: 2019-09-28
 * Time: 13:01
 */

namespace App\Tests\Controller;

use App\Tests\Util\AssertArrayTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PrimeFactorControllerTest extends WebTestCase
{
    use AssertArrayTrait;

    public function testFactor()
    {
        $client = static::createClient();

        // Missing argument
        $client->request('GET', '/factor');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());

        // Invalid algorithm
        $client->request('GET', '/factor?number=100&alg=rubbish');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());

        // Invalid argument
        $client->request('GET', '/factor?rubbish=1');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());

        // Invalid number
        $client->request('GET', '/factor?number=0');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());

        // Invalid number
        $client->request('GET', '/factor?number=900000000000');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());

        $client->request('GET', '/factor?number=100');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Ok',$result['status']);
        $this->assertArray([2=>2, 5=>2], $result['factors']);

        $client->request('GET', '/factor?number=10404');
        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArray([2=>2,3=>2,17=>2],$result['factors']);

        // Fake alternative algorithm
        $client->request('GET', '/factor?number=10404&alg=FakeAlg');
        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertArray([2=>2,3=>2,17=>2],$result['factors']);
    }
}
