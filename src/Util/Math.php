<?php
/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 16:49
 *
 * https://en.wikipedia.org/wiki/Fermat's_factorization_method
 */

namespace App\Util;

use App\Util\Factor\Gcd;
use App\Util\Factor\PrimeFactorInterface;

class Math
{
    private $max = 1000000;

    public function primeFactors( $number, PrimeFactorInterface $algorithm )
    {
        $result = [];
        if ($number) {
            while ( true ) {
                $factor = $algorithm->primeFactorization($number);
                echo "{$number} - {$factor}\n";
                if (!$factor) {
                    return $result;
                }
                $result[] = $factor;
                $number = $number / $factor;
            }
        }
        return $result;
    }

    public function primeC($number )
    {
        print "\nN={$number}\n";
        $loop = 1;
        $x_fixed = 2;
        $x = 2;
        $size = 2;

        $gcd = new Gcd();
        do {
            printf("----   loop %4d   ----\n", $loop);
            $count = $size;
            do {
                $x = ($x * $x + 1) % $number;
                $factor = $gcd->calculate(abs($x - $x_fixed), $number);
                printf("count = %4d  x = %6d  factor = %d\n", $size - $count + 1, $x, $factor);
            } while (--$count && ($factor == 1));
            $size *= 2;
            $x_fixed = $x;
            $loop++;
        } while ($factor == 1);
        printf("factor is %d\n", $factor);
        return ($factor == $number ? null : $factor);
    }
}