<?php
/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 20:09
 */

namespace App\Util\Factor;

interface PrimeFactorInterface {
    public function primeFactorization(int $number): array;
    public function invalidInput(int $number): string;
}
