<?php
/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 16:44
 */
// tests/Util/CalculatorTest.php
namespace App\Tests\Util;

use App\Util\Factor\DirectSearch;
use App\Util\Factor\Gcd;
use PHPUnit\Framework\TestCase;

class MathTest extends TestCase
{
    use AssertArrayTrait;

//    private function assertArray( $expect, $result )
//    {
//        try {
//            $this->assertEquals(count($expect), count($result));
//            foreach ($expect as $k => $v) {
//                $this->assertEquals($v, $result[$k]);
//            }
//        } catch (ExpectationFailedException $e) {
//            echo "\nExpected\n";
//            foreach ($expect as $k => $v) {
//                echo "{$k} x {$v}\n";
//            }
//            echo "Result\n";
//            foreach ($result as $k => $v) {
//                echo "{$k} x {$v}\n";
//            }
//            throw new ExpectationFailedException($e->getMessage());
//        }
//    }

    public function testGcd()
    {
        $gcd = new Gcd();

        $result = $gcd->calculate(2,4);
        $this->assertEquals(2, $result);

        $result = $gcd->calculate(10,100);
        $this->assertEquals(10, $result);

        $result = $gcd->calculate(55,100);
        $this->assertEquals(5, $result);

        $result = $gcd->calculate(1000,1000);
        $this->assertEquals(1000, $result);

        $result = $gcd->calculate(100, 55);
        $this->assertEquals(5, $result);

        // Test 2 primes
        $result = $gcd->calculate(7,13);
        $this->assertEquals(1, $result);

        // Test non integers
        $result = $gcd->calculate(7.5,13.24);
        $this->assertEquals(1, $result);

    }

    public function testPrimeFactors()
    {
        $direct = new DirectSearch();
        $result = $direct->primeFactorization( 10 );
        $this->assertArray([2=>1,5=>1],$result);

        $result = $direct->primeFactorization( 100 );
        $this->assertArray([2=>2,5=>2],$result);

        $result = $direct->primeFactorization( 821 );
        $this->assertArray([821=>1],$result);

        $result = $direct->primeFactorization( 2437 );
        $this->assertArray([2437=>1],$result);

        $result = $direct->primeFactorization( 10403 );
        $this->assertArray([101=>1,103=>1],$result);

        $result = $direct->primeFactorization( 10404 );
        $this->assertArray([2=>2,3=>2,17=>2],$result);

        $result = $direct->primeFactorization( 533715 );
        $this->assertArray([3=>1,5=>1,7=>1,13=>1,17=>1,23=>1],$result);

        $result = $direct->primeFactorization( 1040404 );
        $this->assertArray([2=>2,29=>1,8969=>1],$result);

        $result = $direct->primeFactorization( 104040404 );
        $this->assertArray([2=>2,13=>1,821=>1,2437=>1],$result);

        /**
         * Disabled to speed-up tests
         * @see AbstractPrimeFactor
         */
//        $result = $direct->primeFactorization( 10404040407 );
//        $this->assertArray([3=>1,263=>1,13186363=>1],$result);

        // Not real tests, just preliminary experiments
//        $result = $math->primeC( 10404040407 );
    }
}
