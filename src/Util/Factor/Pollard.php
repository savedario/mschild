<?php
/**
 *
 * User: dario
 * Date: 2019-09-20
 * Time: 20:08
 *
 * https://math.stackexchange.com/questions/631559/algorithms-for-finding-the-prime-factorization-of-an-integer
 * https://www.geeksforgeeks.org/pollards-rho-algorithm-prime-factorization/
 */

namespace App\Util\Factor;

class Pollard extends AbstractPrimeFactor implements PrimeFactorInterface
{
    /**
     * Determine prime factor for an integer
     * Using Pollard-Strassen algorithm
     *
     * @param int $num
     * @return null|array of factors
     */
    public function factorNew( int $num )
    {
        if ($num > $this->max) {
            return null;
        }

        $gcd = new Gcd();

        $c = (int)round(pow((double)$num, 0.25));
        $f = [];
        for ($i = 0; $i < $c; $i++) {
            $f[$i] = 1;
            $jmin = 1 + $i * $c;
            $jmax = $jmin + $c - 1;
            for ($j = $jmin; $j <= $jmax; $j++) {
                $f[$i] = $f[$i] * $j % $num;
            }
        }
        $result = [];
        for ($i = 0; $i < $c; $i++) {
            $factor = $gcd->calculate($f[$i], $num);
            if ($factor != 1) {
                $result[$i] = $factor;
                break;
            }
        }
        return $result;
    }

    public function primeFactorization(int $number ): array
    {
        print "\nN={$number}\n";
        $loop = 1;
        $x_fixed = 2;
        $x = 2;
        $size = 2;

        $gcd = new Gcd();
        do {
            printf("----   loop %4d   ----\n", $loop);
            $count = $size;
            do {
                $x = ($x * $x + 1) % $number;
                $factor = $gcd->calculate(abs($x - $x_fixed), $number);
                printf("count = %4d  x = %6d  factor = %d\n", $size - $count + 1, $x, $factor);
            } while (--$count && ($factor == 1));
            $size *= 2;
            $x_fixed = $x;
            $loop++;
        } while ($factor == 1);
        printf("factor is %d\n", $factor);
        return ($factor == $number ? null : $factor);

    }
}
/*
#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b)
{
    int remainder;
    while (b != 0) {
        remainder = a % b;
        a = b;
        b = remainder;
    }
    return a;
}

int main (int argc, char *argv[])
{
    int number = 10403, loop = 1, count;
    int x_fixed = 2, x = 2, size = 2, factor;

    do {
        printf("----   loop %4i   ----\n", loop);
        count = size;
        do {
            x = (x * x + 1) % number;
            factor = gcd(abs(x - x_fixed), number);
            printf("count = %4i  x = %6i  factor = %i\n", size - count + 1, x, factor);
        } while (--count && (factor == 1));
        size *= 2;
        x_fixed = x;
        loop = loop + 1;
    } while (factor == 1);
    printf("factor is %i\n", factor);
    return factor == number ? EXIT_FAILURE : EXIT_SUCCESS;
}
 */