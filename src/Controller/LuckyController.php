<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class LuckyController extends AbstractController
{
    /**
     * @Route("/home",name="home")
     * @return Response
     * @throws \Exception
     */
    public function index(LoggerInterface $logger)
    {
        $logger->debug('debug'.print_r($_SERVER,true));
        return new Response(
            '<html><body>Home and a bit and some and.<br/>'. phpinfo() .'</body></html>'
        );
    }
    /**
     * @Route("/lucky/number",name="lucky_number")
     * @return Response
     * @throws \Exception
     */
    public function number()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}
