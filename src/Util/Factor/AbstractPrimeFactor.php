<?php
/**
 *
 * User: dario
 * Date: 2019-09-26
 * Time: 18:34
 */

namespace App\Util\Factor;


abstract class AbstractPrimeFactor implements PrimeFactorInterface
{
    /**
     * @var int Max input acceptable
     * Inputs < 10^9 are processed < 1 sec.
     */
    protected $max = 100000000000; // 10^11 or 100G

    public function invalidInput(int $number ): string
    {
        if ($number AND $number <= $this->max) {
            return false;
        }
        return "Number must be > 0 and < {$this->max}";
    }
}
